package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.BowlingException;

public class FrameTest {

	@Test
	public void testGetThrows() throws BowlingException{
		
		Frame frame = new Frame(2, 4);
		
		assertEquals(2,frame.getFirstThrow());
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void testGetErrorThrows() throws BowlingException{
		
		Frame frame = new Frame(2, -4);
		
		assertEquals("Value not correct.",frame.getSecondThrow());
	}
	
	@Test
	public void testGetScore() throws BowlingException{
		
		Frame frame = new Frame(2, 4);
		
		assertEquals(6,frame.getScore());
	}

}
