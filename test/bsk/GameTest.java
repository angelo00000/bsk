package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Game;
import tdd.training.bsk.Frame;
import tdd.training.bsk.BowlingException;

public class GameTest {

	@Test
	public void testGetFrameOfAGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		assertEquals(1,game.getFrameAt(0).getFirstThrow());
		assertEquals(5,game.getFrameAt(0).getSecondThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void testGetFrameOfAGameWithMore10Freames() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(2, 0));
	}
	
	@Test
	public void testGetScoreOfAGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(81,game.calculateScore());	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithSpare() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(88,game.calculateScore());	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithStrike() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(94,game.calculateScore());	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithStrikeAndSpare() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103,game.calculateScore());	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithMultipleStrike() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112,game.calculateScore());	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithMultipleSpare() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98,game.calculateScore());	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithSpareAtLastFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		assertEquals(90,game.calculateScore());	
		
	}
	
	@Test(expected = BowlingException.class)
	public void testGetScoreOfAGameWithErrorFirstBonusThrow() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(-7);	
		
	}
	
	@Test
	public void testGetScoreOfAGameWithStrikeAtLastFrame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92,game.calculateScore());	
		
	}
	
	@Test(expected = BowlingException.class)
	public void testGetScoreOfAGameWithErrorSecondBonusThrow() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		game.setFirstBonusThrow(-2);
		
	}

	@Test
	public void testGetScoreOfAPerfectGame() throws BowlingException{
		// It initializes an empty bowling game.
		Game game = new Game();
		// It adds 10 frames to this bowling game.
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300,game.calculateScore());	
		
	}
}
