package tdd.training.bsk;
import java.util.ArrayList;


public class Game {

	private int fplus;
	private int splus;
	
	ArrayList<Frame> frames = new ArrayList<>();
	
	/**
	 * It initializes an empty bowling game. 
	 */	
	public Game() {
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() < 10) {
			frames.add(frame);
		}else {
			throw new BowlingException("Too many frames");
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index>=0 && index <=9) {
			return frames.get(index);
		}
		else {
			throw new BowlingException("Index not correct.");
		}
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow>=0 && firstBonusThrow<=10) {
			this.fplus = firstBonusThrow;
		}
		else throw new BowlingException("Value not valid");
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow>=0 && secondBonusThrow<=10) {
			this.splus = secondBonusThrow;
		}
		else throw new BowlingException("Value not valid");
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return fplus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return splus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score=0;
		if(frames.size() <11) {
			score = calculation();
			return score;
		} 
		else {
			throw new BowlingException("Too many frames.");
		}
		
	}

	public int calculation() {
		int score=0;
		for(int i=0;i<frames.size();i++) {
			if(i==8 || i==9) {
				score += lastFramesCalculation(i);
			}
			else if(frames.get(i).isStrike()) {
					if(frames.get(i+1).isStrike()) {
						frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+2).getFirstThrow());
						score += frames.get(i).getScore();
					}
					else {
						frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow());
						score += frames.get(i).getScore();
					}
				}
				else if(frames.get(i).isSpare()) {
						frames.get(i).setBonus(frames.get(i+1).getFirstThrow());
						score += frames.get(i).getScore();
				}
				else {
					score += frames.get(i).getScore();
				}
		}
		return score;
	}
	
	public int lastFramesCalculation(int index) {
		if(index == 8) {
			if(frames.get(index).isStrike()) {
				return frames.get(index).getFirstThrow() + frames.get(index+1).getFirstThrow() + fplus;
			}
			else {
				if(frames.get(index).isSpare()) {
					return frames.get(index).getFirstThrow() + frames.get(index).getSecondThrow() + fplus;
				}
			}
			return frames.get(index).getScore(); 
		}
		else if(frames.get(index).isStrike()) {
			return frames.get(index).getFirstThrow() + frames.get(index).getSecondThrow() + fplus + splus;
		}
		else {
			if(frames.get(index).isSpare()) {
				return frames.get(index).getFirstThrow() + frames.get(index).getSecondThrow() + fplus;
			}
		}
		return frames.get(index).getScore();
	}
}
